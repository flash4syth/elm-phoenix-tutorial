defmodule MyServer.PageController do
  use MyServer.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
